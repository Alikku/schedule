-- local awful = require "awful"
-- local gears = require "gears"
-- local wibox = require "wibox"
-- local beautiful = require "beautiful"
-- local dpi = beautiful.xresources.apply_dpi

-- to copy tables
function table.shallow_copy(t)
  local t2 = {}
  for k,v in pairs(t) do
    t2[k] = v
  end
  return t2
end

local schedule = {}

-- Initialize schedule
schedule.monday_odd = {
    "[Основы информационной безопасности][Практика][Б305]",
    "[Основы административного, уголовного и уголовно-процессуального права][Лекция][242]",
    "[Сети и системы передачи информации][Практика][206]",
    "[Основы административного, уголовного и уголовно-процессуального права][Практика][Б305]",
}

schedule.tuesday_odd = {
    "[Физика][Лекция][315]",
    "[Теория информации и кодирования][Лекция][236]",
    "[Теория информации и кодирования][Практика][236]",
    "[Методы оптимизации][Лекция][236]",
}

schedule.wednesday_odd = {
    "[Физика][Практика][327]",
    "[Философия][Лекция][В]",
    "[Основы информационной безопасности][Лекция][315]",
    "[Методы оптимизации][Лабораторная][239]",
}

schedule.thursday_odd = {
    nil,
    "[Сети и системы передачи информации][Лекция][219]",
    "[Физическая культура][Практика][Спортзал]",
    "[Философия][Практика][A313]",
}

schedule.friday_odd = {
    nil,
    "[Объектно-ориентированное программирование][Лабораторная][239]",
    "[Объектно-ориентированное программирование][Лекция][209]",
    "[Иностранный язык][Практика][327]",
}

schedule.monday_even = table.shallow_copy(schedule.monday_odd)

schedule.tuesday_even = table.shallow_copy(schedule.tuesday_odd) 
schedule.tuesday_even[3] = "[Теория информации и кодирования][Лабораторная][236]"
schedule.tuesday_even[4] = nil

schedule.wednesday_even = table.shallow_copy(schedule.wednesday_odd) 
schedule.wednesday_even[1] = "[Физика][Лабораторная][306]"
schedule.wednesday_even[2] = "[Сети и системы передачи информации][Лабораторная][206]"
schedule.wednesday_even[4] = "[Методы оптимизации][Практика][206]"

schedule.thursday_even = table.shallow_copy(schedule.thursday_odd) 
schedule.thursday_even[1] = "[Методы оптимизации][Лекция][220]"

schedule.friday_even = table.shallow_copy(schedule.friday_odd) 
schedule.friday_even[1] = "[Физическая культура][Практика][Спортзал]"

-- days and subjects declaration
-- first is monday, after 7 goes even week
local days = {
    [1]  = schedule.monday_odd,
    [2]  = schedule.tuesday_odd,
    [3]  = schedule.wednesday_odd,
    [4]  = schedule.thursday_odd,
    [5]  = schedule.friday_odd,
    [8]  = schedule.monday_even,
    [9]  = schedule.tuesday_even,
    [10] = schedule.wednesday_even,
    [11] = schedule.thursday_even,
    [12] = schedule.friday_even,
}

-- time are represented as raw minutes from 00:00
local clock = {
    {480, 575},  -- 08:00-09:35
    {585, 680},  -- 09:45-11:20
    {690, 785},  -- 11:30-13:05
    {800, 895},  -- 13:20-14:55
    {905, 1000}, -- 15:05-16:40
}

-- probably only works with linux date cause of %u and %V
local function _get_day_num()
    local day_num = tonumber(os.date("%u"))
    local week_num = tonumber(os.date("%V"))

    if week_num % 2 == 0 then
        day_num = day_num + 7
    end

    return day_num
end

local function _get_subj_num(curr_time)
    local i = 1
    while (i <= #clock)
    do
        if (clock[i][1] <= curr_time) and (curr_time <= clock[i][2]) then
            return i
        end
        i = i + 1
    end
    
    return nil
end

local function _get_subj_num_nearest(curr_time)
    local i = 1
    while (i <= #clock)
    do
        if (curr_time <= clock[i][1]) and (curr_time <= clock[i][2]) then
            return i
        end
        i = i + 1
    end
    
    return nil
end

-- Functions
function schedule.get_current()
    local day_num = _get_day_num()
    
    if days[day_num] then
        local curr_hour = tonumber(os.date("%H"))
        local curr_min = tonumber(os.date("%M"))
        local curr_time = (curr_hour * 60) + curr_min

        subj_num = _get_subj_num(curr_time)

        if subj_num then
            return days[day_num][subj_num]
        else
            return nil
        end
    else
        return nil
    end
end

function schedule.get_next()
    local day_num = _get_day_num()

    local curr_hour = tonumber(os.date("%H"))
    local curr_min = tonumber(os.date("%M"))
    local curr_time = (curr_hour * 60) + curr_min

    subj_num = _get_subj_num_nearest(curr_time)

    if not subj_num then
        subj_num = 1
        day_num = (day_num + 1) % 14
    end

    while not days[day_num] or not days[day_num][subj_num]
    do
        subj_num = subj_num + 1
        if subj_num > #clock then
            subj_num = 1
            day_num = (day_num + 1) % 14
        end
    end

    return days[day_num][subj_num]
end


function schedule.get_next_min()
    local day_num = _get_day_num()

    local curr_hour = tonumber(os.date("%H"))
    local curr_min = tonumber(os.date("%M"))
    local curr_time = (curr_hour * 60) + curr_min

    subj_num = _get_subj_num_nearest(curr_time)

    local offset

    if not subj_num then
        subj_num = 1
        day_num = (day_num + 1) % 14
        offset = 1440 - curr_time + clock[subj_num][1]
    else
        offset = clock[subj_num][1] - curr_time
    end

    while not days[day_num] or not days[day_num][subj_num]
    do
        subj_num = subj_num + 1
        if subj_num > #clock then
            offset = offset + (clock[subj_num - 1][2] - clock[subj_num - 1][1]) + (1440 - clock[subj_num - 1][2] + clock[1][1])
            subj_num = 1
            day_num = (day_num + 1) % 14
        else
            offset = offset + (clock[subj_num - 1][2] - clock[subj_num - 1][1]) + (clock[subj_num][1] - clock[subj_num - 1][2])
        end
    end

    return offset
end

function schedule.get_break()
    local curr_hour = tonumber(os.date("%H"))
    local curr_min = tonumber(os.date("%M"))
    local curr_time = (curr_hour * 60) + curr_min

    subj_num = _get_subj_num(curr_time)

    if subj_num then
        local break_min = clock[subj_num][1] + 45
        local break_count = break_min - curr_time

        if break_count >= 0 then
            return break_count
        else
            return nil
        end
    else
        return nil
    end
end

function schedule.get_end()
    local subj_num = nil

    local curr_hour = tonumber(os.date("%H"))
    local curr_min = tonumber(os.date("%M"))
    local curr_time = (curr_hour * 60) + curr_min

    subj_num = _get_subj_num(curr_time)

    if subj_num then
        local end_count = clock[subj_num][2] - curr_time

        if end_count >= 0 then
            return end_count
        else
            return nil
        end
    else
        return nil
    end
end

return schedule
