schedule = require "schedule"

local sch_cur = schedule.get_current()
local sch_next = schedule.get_next()
local sch_next_min = schedule.get_next_min()
local sch_break = schedule.get_break()
local sch_end = schedule.get_end()

if sch_cur then
print("Текущая пара: "..sch_cur)
end
if sch_next then
print("Следующая пара: "..sch_next)
end
if sch_next_min then
print("До следующей пары: "..sch_next_min.." минут")
end
if sch_break then
print("До перерыва: "..sch_break.." минут")
end
if sch_end then
print("До конца пары: "..sch_end.." минут")
end
